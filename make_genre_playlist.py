# Script that creates a playlist based on saved albums filtered by genre

import spotipy
from spotipy.oauth2 import SpotifyOAuth
import random
import argparse  
import sys
import time
import string
import os

SPOTIFY_CLIENT_ID = os.environ.get('SPOTIFY_CLIENT_ID') 
SPOTIFY_CLIENT_SECRET = os.environ.get('SPOTIFY_CLIENT_SECRET')
SPOTIFY_REDIRECT_URI = os.environ.get('SPOTIFY_REDIRECT_URI')

def L(obj)->list:
    assert(isinstance(obj,list))
    return obj

def D(obj)->dict:
    assert(isinstance(obj,dict))
    return obj

def any_in(items:list[str], s:str):
    if len(items)==0:
        return True
    for item in items:
        if item in s:
            return True
    return False

def make_genre_playlist(genre:list[str], playlist_name:str,
        spotify_client_id,
        spotify_client_secret,
        spotify_redirect_uri): 

    # Set up auth using Authorization Code Flow
    scope = "user-library-read playlist-modify-public playlist-modify-private"
    sp = spotipy.Spotify(
        auth_manager=
        SpotifyOAuth(scope=scope,
                    client_id=spotify_client_id,
                    client_secret=spotify_client_secret,
                    redirect_uri=spotify_redirect_uri))

    # Get saved albums
    offset = 0
    done = False
    albums = []
    webcalls = 0
    while not done: 
        webcalls += 1
        if webcalls % 50 == 0:
            print(f"Completed 50 web calls, taking a nap")
            time.sleep(35)
        response = D(sp.current_user_saved_albums(limit=50, offset=offset))
        items = L(response['items'])
        if len(items)<50:
            done = True
        albums += items
        offset += len(items)
        print(f"Downloaded {offset} albums...")

    print(f"Found {len(albums)} saved albums")

    # Get tracks from saved albums
    num_genre_albums = 0
    all_tracks = []
    for item in albums:
        album = D(D(item)['album'])
        artists = L(album['artists'])
        artists_names = ''
        
        genres = L(album['genres'])
        # Album genres are (almost?) always empty, so also use genres of album artists
        for a in artists:
            artist = D(a)
            artistid = artist['id']
            artists_names = artists_names + artist['name'] + ' '
            webcalls += 1
            if webcalls % 50 == 0:
                print(f"Completed 50 web calls, taking a nap")
                time.sleep(35)
                print("Waking up ...")
            response = D(sp.artist(artistid))
            genres = genres + L(response['genres'])
        
        # Also use the album title, good for compilations
        # Problem: gets lots of jazz albums that are not really blues,
        # so only use if genres is empty
        if genres == []:
            print(f"Warning, no genres found for album {album['name']} so using name as genre")
            genres = [album['name']]
        
        genres_string = ('#'.join(genres)).casefold()

        if any_in(genre, genres_string):
            num_genre_albums += 1
            tracks = D(album['tracks'])
            tracklist = L(tracks['items'])
            print(f"Adding {len(tracklist)} tracks from {artists_names}-- {album['name']} -- {genres_string}")
            for track in tracklist:
                trackid = D(track)['id']
                all_tracks.append(trackid)

    print(f"Number of {genre} albums is {num_genre_albums}")
    print(f"Total number of tracks is {len(all_tracks)}")

    if playlist_name:

        # Get user id - not the same as client id
        profile = D(sp.me())
        my_id = profile['id']

        # Create playlist
        playlist = D(sp.user_playlist_create(user=my_id, name=playlist_name))
        playlist_id = playlist['id']

        print(f"Created playlist {playlist_name}")

        # Shuffle tracks
        random.shuffle(all_tracks)

        # Add all tracks to playlist
        remaining = len(all_tracks)
        if remaining > 10000:
            print(f"Bailing out, Spotify limits playlists to 10,000 tracks", file=sys.stderr)
            exit(1)
        offset = 0
        while remaining>0:
            chunksize = min(50,remaining)
            chunk = all_tracks[offset:offset+chunksize]

            webcalls += 1
            if webcalls % 50 == 0:
                print(f"Completed 50 web calls, taking a nap")
                time.sleep(35)
                print("Waking up ...")

            sp.playlist_add_items(playlist_id=playlist_id, items=chunk)
            remaining -= chunksize
            print(f"Uploaded {chunksize} tracks, {remaining} remain")

        print(f"Added tracks to playlist {playlist_name}")



def main():
    parser = argparse.ArgumentParser(description="Create playlist from saved albums filtered by genre")
    parser.add_argument('--save', '-s', action='store_true')
    parser.add_argument('--name', '-n', type=str, default='')
    parser.add_argument('--genre', '-g', action='extend', default=[], type=str, nargs='+')

    args = parser.parse_args()
    save:str = args.save
    genre:list[str] = args.genre
    name:str = args.name 

    if not name:
        if genre:
            name = 'My ' + string.capwords(' '.join(genre)) + ' Albums'
        else:
            name = 'All My Albums'
    if not save:
        name = ''
    genre = [ s.casefold() for s in genre ]
    make_genre_playlist(genre, name,
        SPOTIFY_CLIENT_ID,
        SPOTIFY_CLIENT_SECRET,
        SPOTIFY_REDIRECT_URI)

if __name__ == '__main__':
    main()

