# make_genre_playlist

Create a playlist from saved albums filtering by genre.

## Use

    python make_genre_playlist.py --genre alternative indie --name 'My Favorite Blues' --save

* Example creates playlist if genre matches "alternative" or "indie".
* If no genre specified, then all tracks from all saved albums will appear in the playlist.
* If the name is not specified, then a name is created based on the genre or lack of genre.
* If -saved is omitted, then the playlist is not saved to the Spotify account.

## Required Python packages

Install with conda or pip:

    conda install spotipy argparse

## Registering a Spotify app

Follow [these steps](https://developer.spotify.com/documentation/general/guides/authorization/app-settings/) to
create an app and get a client ID, a client secret, and redirect URI (an arbitrary URL).

Set shell variables to these values:

    export SPOTIFY_CLIENT_ID="some string"
    export SPOTIFY_CLIENT_SECRET="some string"
    export SPOTIFY_REDIRECT_URI="http://localhost:8227/"



